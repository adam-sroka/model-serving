from flask import Flask

from serving.sqlalchemydb import db
from serving.views import views


def create_app():
    app = Flask(__name__)
    app.register_blueprint(views)
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///config.db'
    db.init_app(app)
    return app


if __name__ == '__main__':
    app = create_app()
    app.run(debug=True)
