from flask import Blueprint, render_template, request, redirect

from serving.model import Model
from serving.models import Engine
from serving.sqlalchemydb import db

views = Blueprint('views', __name__, template_folder='templates')


@views.route('/')
def index():
    return render_template('index.html')


@views.route('/add', methods=['GET', 'POST'])
def add_engine():
    if request.method == 'POST':
        engine = Engine(**request.form.to_dict())
        if not engine.name:
            return redirect('/add')

        try:
            db.session.add(engine)
            db.session.commit()
            return render_template('add_engine.html', success='Engine save successfully')
        except:
            return render_template('add_engine.html', error='Error occurred while saving engine to DB!')
    else:
        return render_template('add_engine.html')


@views.route('/list')
def list_engines():
    engines = Engine.query.all()
    return render_template('list_engines.html', engines=engines)
