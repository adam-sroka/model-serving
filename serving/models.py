from datetime import datetime

from serving.sqlalchemydb import db


class Engine(db.Model):
    __tablename__ = 'engines'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False, unique=True)
    algorithm = db.Column(db.String(50), nullable=False)
    train_start = db.Column(db.DateTime, default=datetime.fromtimestamp(0))
    created = db.Column(db.DateTime, default=datetime.utcnow())

    def __init__(self, name: str, algorithm: str, train_start: datetime):
        self.name = name
        self.algorithm = algorithm
        self.train_start = datetime.strptime(train_start, '%Y-%m-%d') if train_start else datetime.fromtimestamp(0)

    def __repr__(self):
        return f'<Engine {self.__dict__}>'
