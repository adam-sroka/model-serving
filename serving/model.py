from uuid import uuid4


class Model:
    @classmethod
    def get_recommendations(cls, count: int):
        return [str(uuid4())[:8] for _ in range(int(count))]

